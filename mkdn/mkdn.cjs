const MARKDOWN_DIRECTORY = 'mkdn/md';
const POST_DIRECTORY = 'posts';

var fs = require('fs');
var hljs = require('highlight.js');
var md = require('markdown-it')({
  highlight: function(str, lang) {
    if (lang && hljs.getLanguage(lang)) {
      try {
        return `<pre class="hljs rounded" style="padding: 8px;"><code>${hljs.highlight(str, { language: lang, ignoreIllegals: true }).value}</code></pre>`
      } catch (__) { }
    }
    return `<pre class="hljs rounded"><code>${md.utils.escapeHtml(str)}</code></pre>`
  }
});

var urls = [];
var files = fs.readdirSync(MARKDOWN_DIRECTORY);
var template_text = fs.readFileSync('mkdn/post_template.html').toString('utf-8');

var files_written = [];
files.forEach((f) => {
  var file_dir = `${MARKDOWN_DIRECTORY}/${f}`;
  var file_name = f.split('.')[0];
  var out_dir = `${POST_DIRECTORY}/${file_name}.html`;

  if (fs.statSync(file_dir).isFile()) {
    var content = fs.readFileSync(file_dir).toString('utf-8');
    var rendered = template_text.replace('<!-- REPLACE -->', md.render(content));
    fs.writeFileSync(out_dir, `<!DOCTYPE html>\r\n<meta name="viewport" content="width=device-width, initial-scale=1" />\r\n ${rendered}`);

    urls.push({
      url: `/posts/${file_name}.html`,
      filename: file_name,
    });
    files_written.push(out_dir);
  }
});

fs.writeFileSync('urls.txt', JSON.stringify(urls));

fs.cpSync('posts', 'dist/posts', { recursive: true });
fs.cpSync('lineawesome', 'dist/lineawesome', { recursive: true });
fs.cpSync('urls.txt', 'dist/urls.txt');

console.log('HTML markup generated:');
files_written.forEach((f) => {
  console.log(`-- ${f}`);
})
