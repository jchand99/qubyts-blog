const config = {
  content: ["./index.html", "./src/**/*.{html,js,svelte,ts}", "./posts/*.html"],
  darkMode: 'class',
  theme: {
    extend: {},
  },
  plugins: [],
};

module.exports = config;
